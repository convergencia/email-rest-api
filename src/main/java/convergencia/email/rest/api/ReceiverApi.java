/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convergencia.email.rest.api;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Wildes
 */
@Component
@Path("/receive")
@Produces("application/json")
public class ReceiverApi {

    @Autowired
    private SessionTemplateFactory stf;

    @GET
    public List<Integer> listUnreadedMessages(@Context HttpHeaders httpHeaders, @QueryParam("topic") String topic) {
        String user = httpHeaders.getHeaderString("public");
        String password = httpHeaders.getHeaderString("secret");

        try (SessionTemplate st = stf.create(user, password)) {
            return st.listUnreadedMessages(topic);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }

    @GET
    @Path("/read")
    public String getMessage(@Context HttpHeaders httpHeaders, @QueryParam("topic") String topic, @QueryParam("msgId") Integer msgId) {
        String user = httpHeaders.getHeaderString("public");
        String password = httpHeaders.getHeaderString("secret");

        try (SessionTemplate st = stf.create(user, password)) {
            return st.getMessage(topic, msgId);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}
