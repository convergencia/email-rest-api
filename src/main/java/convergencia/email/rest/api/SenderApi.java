/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convergencia.email.rest.api;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Wildes
 */
@Component
@Path("/send")
@Produces("application/json")
public class SenderApi {

    @Autowired
    private SessionTemplateFactory stf;

    @POST
    public void sendMessage(@Context HttpHeaders headers, String body) {
        String user = headers.getHeaderString("public");
        String password = headers.getHeaderString("secret");

        try (SessionTemplate sessionTemplate = stf.create(user, password)) {
            sessionTemplate.sendMessage(body);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}
