/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convergencia.email.rest.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.mail.Flags;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.search.FlagTerm;

/**
 *
 * @author Wildes
 */
public class SessionTemplate implements AutoCloseable {

    private final Session session;

    private Store store;
    private Transport transport;

    public SessionTemplate(Properties props) {
        this.session = Session.getInstance(props);
    }

    private void connectStore() {
        if (store == null || !store.isConnected()) {
            try {
                store = session.getStore();
                store.connect(getUser(), getPassword());
            } catch (NoSuchProviderException ex) {
                throw new RuntimeException(ex);
            } catch (MessagingException ex) {
                throw new RuntimeException(ex);
            }
        }

    }

    public String getUser() {
        return session.getProperty("mail.user");
    }

    public String getPassword() {
        return session.getProperty("mail.password");
    }

    private void connectTransport() {
        if (transport == null || !transport.isConnected()) {
            try {
                transport = session.getTransport();
                transport.connect(getUser(), getPassword());
            } catch (NoSuchProviderException ex) {
                throw new RuntimeException(ex);
            } catch (MessagingException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    @Override
    public void close() throws Exception {
        if (store != null) {
            store.close();
        }

        if (transport != null) {
            transport.close();
        }
    }

    public List<String> listFolders() {

        try {
            connectStore();
            Folder rootFolder = store.getDefaultFolder();
            Folder[] folders = rootFolder.list();
            List<String> folderNames = null;
            if (folders != null) {
                folderNames = new ArrayList<>();
                for (Folder i : folders) {
                    folderNames.add(i.getName());
                }
            }
            return folderNames;
        } catch (NoSuchProviderException ex) {
            throw new RuntimeException(ex);
        } catch (MessagingException ex) {
            throw new RuntimeException(ex);
        }

    }

    public List<Integer> listMessages(String folderName) {
        try {
            connectStore();
            List<Integer> messageList = null;
            Folder folder = store.getFolder(folderName);
            folder.open(Folder.READ_ONLY);
            Message[] messages = folder.getMessages();

            if (messages != null) {
                messageList = new ArrayList<>();
                for (Message m : messages) {
                    Integer messageNumber = m.getMessageNumber();
                    messageList.add(messageNumber);
                }
            }
            return messageList;
        } catch (MessagingException ex) {
            throw new RuntimeException();
        }
    }

    public void sendMessage(String body) {
        try {
            connectTransport();

            String from = getUser();
            String to = from;

            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject("JavaMail APIs Test");
            msg.setSentDate(new Date());
            msg.setText(body);

            transport.sendMessage(msg, address);

        } catch (MessagingException ex) {
            throw new RuntimeException(ex);
        }
    }

    public String getMessage(String folderName, Integer msgNumber) {
        try {
            connectStore();
            Folder folder = store.getFolder(folderName);
            folder.open(Folder.READ_ONLY);
            Message message = folder.getMessage(msgNumber);
            return message.getContent().toString();
        } catch (MessagingException | IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public List<Integer> listUnreadedMessages(String folderName) {
        try {
            List<Integer> messageList = null;
            connectStore();
            Folder folder = store.getFolder(folderName);
            folder.open(Folder.READ_ONLY);
            Message[] messages = folder.search(new FlagTerm(new Flags(Flag.SEEN), false));
            if (messages != null) {
                messageList = new ArrayList<>();
                for (Message m : messages) {
                    Integer messageNumber = m.getMessageNumber();
                    messageList.add(messageNumber);
                }
            }

            return messageList;
        } catch (MessagingException ex) {
            throw new RuntimeException(ex);
        }
    }

}
