/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convergencia.email.rest.api;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 *
 * @author Wildes
 */
@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(SenderApi.class);
        register(ReceiverApi.class);
        register(ExplorerApi.class);

    }

}
