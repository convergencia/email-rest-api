/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convergencia.email.rest.api;

import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Wildes
 */
@Component
public class SessionTemplateFactory {

    @Autowired
    private SessionConfig config;

    public SessionTemplate create(String user, String password) {
        Properties props = new Properties();
        props.putAll(config.getJavax());
        props.put("mail.user", user);
        props.put("mail.password", password);
        return new SessionTemplate(props);
    }

}
