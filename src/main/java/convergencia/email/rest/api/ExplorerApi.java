/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package convergencia.email.rest.api;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Wildes
 */
@Component
@Path("/explore")
@Produces("application/json")
public class ExplorerApi {

    @Autowired
    private SessionTemplateFactory stf;

    @GET
    public List<String> listFolders(@Context HttpHeaders headers) {
        String user = headers.getHeaderString("public");
        String password = headers.getHeaderString("secret");
        try (SessionTemplate sessionTemplate = stf.create(user, password)) {
            return sessionTemplate.listFolders();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @GET
    @Path("/{folder}")
    public List<Integer> listMessages(@Context HttpHeaders headers, @PathParam("folder") String folderName) {
        String user = headers.getHeaderString("public");
        String password = headers.getHeaderString("secret");
        try (SessionTemplate sessionTemplate = stf.create(user, password)) {
            return sessionTemplate.listMessages(folderName);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}
